import TrichterDB, { Group } from '@trichter/db'
import * as moment from 'moment'
import 'moment-timezone'
import * as Twig from 'twig'
import * as path from 'path'
import { URL } from 'url'
import Logger from '@trichter/logger'
import { promises as fs } from 'fs'

import { transformEvents, groupEventsByDate, EventEntry } from './utils/events'
// import getExistingEventFiles from './utils/existingEventFiles'
import './utils/twigFilter'
moment.locale('de')
moment.tz.setDefault('Europe/Berlin')
const log = Logger('trichter-builder')

const repoPath = process.argv[2]
if (!repoPath || !repoPath.trim()) {
  log.error('no repo path provided')
  process.exit(1)
}
const outputPath = path.join(__dirname, '../output')
const db = new TrichterDB(repoPath)

const templates = {
  start: Twig.twig({
    id: 'start',
    path: path.join(__dirname, 'templates/startsite.twig'),
    async: false
  }),
  event: Twig.twig({
    id: 'event',
    path: path.join(__dirname, 'templates/event.twig'),
    async: false
  }),
  info: Twig.twig({
    id: 'info',
    path: path.join(__dirname, 'templates/info.twig'),
    async: false
  }),
  datenschutz: Twig.twig({
    id: 'datenschutz',
    path: path.join(__dirname, 'templates/datenschutz.twig'),
    async: false
  }),
  browseMonth: Twig.twig({
    id: 'browse-month',
    path: path.join(__dirname, 'templates/browse-month.twig'),
    async: false
  }),
  groups: Twig.twig({
    id: 'groups',
    path: path.join(__dirname, 'templates/groups.twig'),
    async: false
  })
}

async function storeGroupImage (group: Group): Promise<void> {
  if (!group.imageFile) return
  const imagePath = `images/${group.key}/group${path.extname(group.imageFile.filename)}`
  const filePath = path.join(outputPath, imagePath)
  await fs.writeFile(filePath, group.imageFile.toBuffer())
}

async function generateEvent (data: {
  event: EventEntry
  browseLink: string
}): Promise<void> {
  const { event, browseLink } = data
  let imagePath = null
  if (event.imageFile) {
    const image = event.imageFile
    imagePath = `images/${event.groups[0].key}/${event.image}`

    const filePath = path.join(outputPath, imagePath)
    await fs.writeFile(filePath, image.toBuffer())
  }

  const upcomingLinkedDates = event.linkedDates.filter(e => {
    return moment(e.start).isAfter(moment())
  })
    .sort((a: EventEntry, b: EventEntry) => {
      return a.start.valueOf() - b.start.valueOf()
    })

  const url = event.link ? new URL(event.link) : null

  const rendered = (await templates.event.render({
    title: `${event.title} | ulmtreiben.de | Veranstaltungen in Ulm`,
    browseLink: browseLink,
    event: event,
    upcomingLinkedDates: upcomingLinkedDates,
    imagePath: imagePath,
    domain: url ? url.hostname.replace(/^www\./, '') : null,
    og: {
      image: event.image,
      title: `${event.title} | ulmtreiben.de`,
      description: event.teaser,
      url: `https://ulmtreiben.de/${event.path}`
    }
  }))
    .replace(/\s{2,}/g, ' ') // simple minifier

  await fs.writeFile(path.join(outputPath, event.path), rendered)
}

async function generateGroupsPage (data: {
  groups: Group[]
  browseLink: string
}): Promise<void> {
  const { groups, browseLink } = data
  const rendered = (await templates.groups.render({
    title: 'Gruppen | ulmtreiben.de | Veranstaltungen in Ulm',
    browseLink: browseLink,
    groups: groups.sort((a, b) => a.name.localeCompare(b.name))
  }))
    .replace(/\s{2,}/g, ' ') // simple minifier

  await fs.writeFile(path.join(outputPath, 'gruppen.html'), rendered)
}

async function generateBrowsePages (data: {
  monthsWithEvents: string[]
  daysWithEvents: string[]
  eventsByDate: {[date: string]: EventEntry[]}
  browseLink: string
}): Promise<void> {
  for (const month of data.monthsWithEvents) {
    const year = month.slice(0, 4)
    const nameOfMonth = moment(month).format('MMMM')
    const rendered = templates.browseMonth.render({
      title: `${nameOfMonth} ${year} | ulmtreiben.de | Veranstaltungen in Ulm`,
      monthKey: month,
      months: data.monthsWithEvents.map(m => ({
        key: m,
        name: moment(m).format('MMMM'),
        year: m.slice(0, 4),
        link: `/${m.slice(0, 4)}/${moment(m).format('MMMM').toLowerCase()}.html`
      })),
      days: data.daysWithEvents.filter(d => d.indexOf(month) === 0),
      eventsByDate: data.eventsByDate,
      browseLink: data.browseLink
    })
    await fs.writeFile(path.join(outputPath, year, `${nameOfMonth.toLowerCase()}.html`), rendered)
  }
}

(async function main () {
  try {
    const branch = await db.checkoutBranch('master')
    await branch.loadData()
    log.debug(`loaded ${branch.events.length} events in ${branch.groups.length} groups`)

    try {
      log.debug('ceate events directory')
      await fs.mkdir(path.join(outputPath, 'events'))
    } catch (err) {}

    // log.debug('get existing event files')
    // const existingEventFiles = await getExistingEventFiles(outputPath)

    log.debug('group and transform events')
    const events = transformEvents(branch.events)
    const futureEventsByDate = groupEventsByDate(
      events
        .filter(e => e.end ? moment(e.end).isAfter(moment()) : moment(e.start).add(2, 'hours').isAfter(moment()))
        .filter(e => moment(e.start).add(3, 'days').isAfter(moment()))
    )

    const eventsByDate = groupEventsByDate(
      events
    )
    const daysWithEvents = Object.keys(eventsByDate).sort()
      .filter(day => {
        // ignore everything before 6 months ago
        if (moment(day).add(6, 'months').isBefore(moment())) return false

        // ignore everthing more than 6 months in the future
        if (moment(day).subtract(6, 'months').isAfter(moment())) return false
        return true
      })
    const monthsWithEvents = daysWithEvents.map(d => d.substr(0, 7)).filter((v, i, self) => self.indexOf(v) === i).sort()
    const yearsWithEvents = Object.keys(eventsByDate).sort().map(d => d.substr(0, 4)).filter((v, i, self) => self.indexOf(v) === i).sort()

    const browseLink = `/${moment().year()}/${moment().format('MMMM').toLowerCase()}.html`

    log.info('create folders...')
    for (const year of yearsWithEvents) {
      try {
        await fs.mkdir(path.join(outputPath, year))
      } catch (err) {}
    }
    for (const group of branch.groups) {
      try {
        await fs.mkdir(path.join(outputPath, 'images', group.key), { recursive: true })
      } catch (err) {
        console.log(err)
      }
    }

    log.info('store group images...')
    for (const group of branch.groups) {
      await storeGroupImage(group)
    }

    log.info('generate group page...')
    await generateGroupsPage({
      groups: branch.groups,
      browseLink
    })

    log.debug('generate browse pages...')
    await generateBrowsePages({
      monthsWithEvents,
      daysWithEvents,
      eventsByDate,
      browseLink
    })

    log.info('generate event pages...')
    for (const event of events) {
      // if(!event.path.match(/cq0u|aufstand/)) continue
      log.debug(`generate ${event.path}`)
      await generateEvent({
        event,
        browseLink
      })
    }

    log.info('generate index...')
    const rendered = (await templates.start.render({
      title: 'ulmtreiben.de | Veranstaltungen in Ulm',
      days: Object.keys(futureEventsByDate).sort().slice(0, 10),
      eventsByDate: futureEventsByDate,
      browseLink
    }))
      .replace(/\s{2,}/g, ' ') // simple minifier
    await fs.writeFile(path.join(outputPath, 'index.html'), rendered)

    log.info('generate info...')
    const rendered2 = templates.info.render({
      title: 'ulmtreiben.de | Veranstaltungen in Ulm',
      days: Object.keys(futureEventsByDate).sort().slice(0, 10),
      eventsByDate: futureEventsByDate,
      browseLink
    })
    await fs.writeFile(path.join(outputPath, 'info.html'), rendered2)

    log.info('generate datenschutz...')
    const rendered3 = templates.datenschutz.render({
      title: 'ulmtreiben.de | Veranstaltungen in Ulm',
      days: Object.keys(futureEventsByDate).sort().slice(0, 10),
      eventsByDate: futureEventsByDate,
      browseLink
    })
    await fs.writeFile(path.join(outputPath, 'datenschutz.html'), rendered3)

    log.info('done')
    process.exit(0)
  } catch (err) {
    log.error(err)
  }
}())
