import { Group, Event, ImageFile } from '@trichter/db'
import * as moment from 'moment'
import * as crypto from 'crypto'
import 'moment-recur-ts'

export interface EventEntry extends Partial<Event> {
  hash: string
  path: string
  id: string
  title: string
  start: Date
  end: Date
  locationName: string
  address: string
  link: string
  image: string
  teaser: string
  isCrawled: boolean
  isRecurredEntry: boolean
  groups: Group[]
  eventFiles: Event[]
  linkedDates: EventEntry[]
  description: string
  imageFile?: ImageFile
}

function getFirstExistingValue (objects: any[], key: string): any {
  if (!objects.length) return undefined
  for (const obj of objects) {
    if (obj[key]) return obj[key]
  }
  return objects[0][key]
}
export function slugify (str: string): string {
  return str
    .toLowerCase()
    .replace(/ä/g, 'ae')
    .replace(/ö/g, 'oe')
    .replace(/ü/g, 'ue')
    .replace(/ß/g, 'ss')
    .replace(/[^a-z0-9]/g, '-')
    .replace(/-+/g, '-')
    .replace(/(^-|-$)/g, '')
}

function getHash (str: string): string {
  return crypto.createHash('sha1').update(str, 'utf8').digest('base64').replace(/[^a-zA-Z0-9]/g, '')
}

export function transformEvents (events: Event[]): EventEntry[] {
  const res: EventEntry[] = []

  // deduplicate events
  // (duplicated events are identified by name + start time)
  const eventsByKey: {[key: string]: Event[]} = {}
  for (const event of events) {
    const key = event.title + event.start.valueOf().toString()
    if (eventsByKey[key]) eventsByKey[key].push(event)
    else eventsByKey[key] = [event]
  }
  for (const groupedEvents of Object.values(eventsByKey)) {
    const first = groupedEvents[0]

    // prefer links without facebook
    const withoutFacebook = groupedEvents.filter(e => e.link && !e.link.startsWith('https://www.facebook.com'))
    const link: string = withoutFacebook.length ? getFirstExistingValue(withoutFacebook, 'link') : getFirstExistingValue(groupedEvents, 'link')

    const title = getFirstExistingValue(groupedEvents, 'title')
    const hash = getHash(first.id + link)
    res.push({
      hash: hash,
      path: `${first.start.getFullYear()}/${slugify(title)}-${hash.slice(0, 4).toLowerCase()}.html`,
      id: first.id,
      title: title,
      start: first.start,
      end: first.end,
      locationName: getFirstExistingValue(groupedEvents, 'locationName'),
      address: getFirstExistingValue(groupedEvents, 'address'),
      description: getFirstExistingValue(groupedEvents, 'description'),
      link: link,
      image: getFirstExistingValue(groupedEvents, 'image'),
      teaser: getFirstExistingValue(groupedEvents, 'teaser'),
      isCrawled: getFirstExistingValue(groupedEvents, 'isCrawled'),
      imageFile: getFirstExistingValue(groupedEvents, 'imageFile'),
      isRecurredEntry: false,
      groups: groupedEvents.map(e => e.group).filter(e => e.key !== '_'),
      eventFiles: groupedEvents,
      linkedDates: []
    })
  }

  // detect and link external recurring events
  for (const a of res) {
    for (const b of res) {
      if (a.id === b.id) continue // don't compare with the same event
      if (a.parentId && b.parentId) {
        if (a.parentId === b.parentId) {
          a.linkedDates.push(b)
        }
      } else if (a.title === b.title) {
        a.linkedDates.push(b)
      }
    }
  }

  // duplicate recurring events
  for (const event of res) {
    if (event.isRecurredEntry) continue
    if (!event.eventFiles[0].recurring) continue

    event.linkedDates.push(event)
    // @ts-ignore
    const recur = moment(event.start).recur(
      Object.assign(
        {
          start: moment(event.start),
          end: moment(event.start).add('1', 'year')
        },
        event.eventFiles[0].recurring
      )
    )
    const dates: moment.Moment[] = recur.next(200)
    const diff = event.end ? moment(event.end).diff(event.start, 'minutes') : null
    dates.unshift(moment(event.start))

    for (const date of dates) {
      // generated date before now-1d? -> ignore
      if (date.isBefore(moment().subtract(1, 'day'))) continue

      // only generate upcoming dates in the next 3 months
      if (date.isAfter(moment().add(3, 'months'))) continue

      const clone = Object.assign({}, event)
      clone.start = date.hours(moment(event.start).hours()).toDate()

      // same date as original? -> ignore
      if (clone.start.valueOf() === event.start.valueOf()) continue

      const hash = getHash(clone.id + clone.link + date.toString())
      clone.path = `${clone.start.getFullYear()}/${slugify(clone.title)}-${hash.slice(0, 4).toLowerCase()}.html`
      if (this.end) clone.end = moment(date).add(diff, 'minutes').toDate()
      clone.isRecurredEntry = true
      event.linkedDates.push(clone)
      res.push(clone)
    }
  }

  return res
}

export function groupEventsByDate (events: EventEntry[]): {[date: string]: EventEntry[]} {
  const dates: {[date: string]: EventEntry[]} = {}

  for (const event of events) {
    const date = moment(event.start).utc().format('YYYY-MM-DD')
    if (!dates[date]) dates[date] = []
    dates[date].push(event)
  }

  // sort by time
  for (const date in dates) {
    dates[date] = dates[date]
      .sort((a, b) => a.end && b.end ? a.end.valueOf() - b.end.valueOf() : 0)
      .sort((a, b) => a.start.valueOf() - b.start.valueOf())
  }

  return dates
}
